from django.conf.urls import url, include
from apps.schedule import urls as schedule_urls

urlpatterns = [
    url(r'^', include(schedule_urls, namespace="schedule")),
]
