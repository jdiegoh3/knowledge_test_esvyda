# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-13 02:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0002_auto_20180212_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pacient',
            name='birth_date',
            field=models.DateField(),
        ),
    ]
