# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-13 14:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0012_auto_20180213_0952'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='appointment_type',
            new_name='appointment_types',
        ),
        migrations.RemoveField(
            model_name='appointment',
            name='type',
        ),
        migrations.AddField(
            model_name='appointment',
            name='type',
            field=models.ManyToManyField(to='schedule.appointment_type'),
        ),
    ]
