from rest_framework.serializers import ModelSerializer
from apps.schedule.models import pacient, appointment

class AppointmentSerializer(ModelSerializer):
	class Meta:
		model = appointment
		fields = ('date', 'type', 'pacient')

class PacientSerializer(ModelSerializer):
	class Meta:
		model = pacient
		fields = ('name', 'birth_date', 'telephone')

