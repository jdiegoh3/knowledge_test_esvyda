from django import forms
from apps.schedule.models import appointment, appointment_types, pacient
from django.contrib.admin import widgets

class create_appointment_form(forms.ModelForm):
    date = forms.TextInput(attrs={'class': 'datepicker'})
    class Meta:
        model = appointment
        fields = [
            'date',
            'type',
            'pacient',
        ]
        labels = {
            'date': 'Fecha de la cita:',
            'type': 'Tipo de cita:',
            'pacient' : 'Paciente asociado:',
        }

    def __init__(self, *args, **kwargs):
        super(create_appointment_form, self).__init__(*args, **kwargs)
        types = []
        query = appointment_types.objects.all().values_list('id', flat=True)
        for id in query:
            types.append((id, appointment_types.objects.get(id=id).type))
        types = tuple(types)
        self.fields["type"].widget = forms.widgets.RadioSelect(choices=types)
        pacients=[]
        query = pacient.objects.all().values_list('id', flat=True)
        for id in query:
            pacients.append((id, pacient.objects.get(id=id).name))
        pacients = tuple(pacients)
        self.fields["pacient"].widget = forms.widgets.RadioSelect(choices=pacients)

class PacientForm(forms.ModelForm):

	class Meta:
		model = pacient
		fields = [
			'name',
			'birth_date',
			'telephone',
		]
		labels = {
			'name': 'Nombres:',
			'birth_date': 'Fecha de nacimiento',
			'telephone': 'Edad',
		}
		widgets = {
			'name':forms.TextInput(attrs={'class':'form-control'}),
            'birth_date': forms.TextInput(attrs={'type': 'date'}),
			'telephone':forms.TextInput(attrs={'class':'form-control'}),
        }