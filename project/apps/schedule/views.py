from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from django.views.generic import CreateView, ListView, UpdateView
from apps.schedule.models import appointment, pacient
from apps.schedule.forms import create_appointment_form, PacientForm
from django.core.urlresolvers import reverse_lazy
from apps.schedule.serializers import AppointmentSerializer, PacientSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime
import json

class ListSchedule(ListView):
    model = appointment
    template_name = 'schedule_templates/schedule.html'

    def get_queryset(self):
        objects = appointment.objects.order_by("date")
        for obj in objects:
            if(obj.date.date() < datetime.datetime.now().date()):
                obj.delete()
        return objects


def check_date(date):
    checked=True
    list_appointments = appointment.objects.all()
    attention_time = date + datetime.timedelta(minutes=-15)
    for appointment_x in list_appointments:
        if(date.date() == appointment_x.date.date()):
            if((date.hour == appointment_x.date.hour and date.minute == appointment_x.date.minute) or (attention_time.hour == appointment_x.date.hour and attention_time.minute <= appointment_x.date.minute)):
                checked=False
    if(checked):
        return True
    else:
        return False


class AppointmentAPI(APIView):
    serializer = AppointmentSerializer

    def get(self, request, format=None):
        objects = appointment.objects.all()
        response = self.serializer(objects, many=True)
        return Response(response.data, status=status.HTTP_200_OK)

class PacientsAPI(APIView):
    serializer = PacientSerializer

    def get(self, request, format=None):
        objects = pacient.objects.all()
        response = self.serializer(objects, many=True)
        return Response(response.data, status=status.HTTP_200_OK)

class ListPacients(ListView):
    model = pacient
    template_name = 'schedule_templates/admin_pacients.html'

class UpdatePacient(UpdateView):
    model = pacient
    template_name = 'schedule_templates/update_pacient.html'
    form_class = PacientForm
    success_url = reverse_lazy('schedule:admin_pacients')

    def get_context_data(self, **kwargs):
        context = super(UpdatePacient, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        solicitud = self.model.objects.get(id=pk)
        if 'form' not in context:
            context['form'] = self.form_class()
        context['id'] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        id_solicitud = kwargs['pk']
        solicitud = self.model.objects.get(id=id_solicitud)
        form = self.form_class(request.POST, instance=solicitud)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return HttpResponseRedirect(self.get_success_url())

def create_appointment(request):
    if request.method == "GET":
        today = datetime.datetime.now()
        form = create_appointment_form()
        return render(request, "schedule_templates/create_appointment.html", {"form": form, "date_today" : today.date().isoformat()})
    elif(request.method == "POST"):
        form = create_appointment_form(request.POST)
        date = request.POST.get("date").split("-")
        time = request.POST.get("time").split(":")
        if(len(time) <= 2):
            time.append("00")
        try:
            datetime_value = datetime.datetime(
                                                int(date[0]),int(date[1]),int(date[2]),
                                                int(time[0]), int(time[1], int(time[2]))
                                              )
            if(check_date(datetime_value)):
                if(datetime_value.date() >= datetime.datetime.now().date()):
                    solicitud = form.save(commit=False)
                    solicitud.date = datetime_value
                    solicitud.save()
                    form.save_m2m()
                    return redirect("schedule:index")
                else:
                    form.errors["date"] = " Estas agendando una cita antes de la fecha de hoy "
                    return render(request, "schedule_templates/create_appointment.html",
                                  {"form": form, "date_today": datetime.datetime.now().date})
            else:
                form.errors["date"] = " Ya hay una cita asignada en ese intervalo "
                return render(request, "schedule_templates/create_appointment.html", {"form": form, "date_today" : datetime.datetime.now().date})
        except:
            return render(request, "schedule_templates/create_appointment.html", {"form": form, "date_today" : datetime.datetime.now().date})
