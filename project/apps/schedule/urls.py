from django.conf.urls import url
from apps.schedule.views import ListSchedule, create_appointment, ListPacients, UpdatePacient, AppointmentAPI, PacientsAPI
urlpatterns = [
    url(r'^$', ListSchedule.as_view(), name="index"),
    url(r'create_appointment$', create_appointment, name="create_appointment"),
    url(r'admin_pacients', ListPacients.as_view(), name="admin_pacients"),
    url(r'^pacient/edit/(?P<pk>\d+)$', UpdatePacient.as_view(), name='update_pacient'),
    url(r'services/appointment', AppointmentAPI.as_view()),
    url(r'services/pacients', PacientsAPI.as_view()),
]
