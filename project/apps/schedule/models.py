from django.db import models

class appointment_types(models.Model):
    type = models.CharField(max_length=20)

class pacient(models.Model):
    name = models.CharField(max_length=80)
    birth_date = models.DateField()
    telephone = models.CharField(max_length=15)

class appointment(models.Model):
    date = models.DateTimeField()
    type = models.ForeignKey(appointment_types, default=None)
    pacient = models.ForeignKey(pacient, default=None)
